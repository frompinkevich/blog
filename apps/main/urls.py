from django.conf.urls import patterns, url

urlpatterns = patterns('apps.main.views',
    # Examples:
    # url(r'^$', 'Blog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^auth/$', 'auth', name='auth'),
    url(r'^logout/$', 'logout', name='logout'),
    url(r'^new_post/$', 'new_post', name='new_post'),
    url(r'^(?P<slug>\w+)/$', 'one_post', name='one_post'),
    url(r'^$', 'index', name='index'),
)
