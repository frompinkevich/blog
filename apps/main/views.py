# coding: utf-8
from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate
from django.core.paginator import Paginator,  EmptyPage, PageNotAnInteger
from django.utils.timezone import datetime

from pytils.translit import translify

from apps.main.models import Posts
from apps.main.forms import NewPostForm

# Create your views here.


def index(request):
    model_posts = Posts.objects.all()
    posts_pages = Paginator(model_posts, 10)
    page = request.GET.get('page')
    try:
        posts = posts_pages.page(page)
    except PageNotAnInteger:
        posts = posts_pages.page(1)
    except EmptyPage:
        posts = posts_pages.page(posts_pages.num_pages)
    return render(request, 'main/index.html', {'posts': posts})


def one_post(request, slug=1):
    try:
        post = Posts.objects.get(slug=slug)
    except:
        post = 'Статья не найдена.'
    return render(request, 'main/post.html', {'post': post})


def new_post(request):
    if request.user.is_authenticated():
        new_post_model = Posts()
        new_post_model.author_id = request.user.id
        new_post_model.created = datetime.now().strftime('%Y-%m-%d')
        form = NewPostForm()
        if request.POST:
            form = NewPostForm(request.POST, request.FILES)
            if form.is_valid():
                new_post_model.title = form.data['title']
                new_post_model.short_body = form.data['body'][:350]
                new_post_model.body = form.data['body']
                new_post_model.image = request.FILES['image']
                slug = translify(new_post_model.title).decode('utf-8')
                # Список замены можно продолжать на боевом проекте и другим способом...
                new_post_model.slug = slug.replace(' ', '_').replace(',', '').\
                    replace('.', '').replace('!', '').replace('?', '').replace('%', '').\
                    replace("'", '').replace('"', '').replace('`', '')
                new_post_model.save()
                return redirect('index')
        return render(request, 'main/new_post.html', {'form': form})
    else:
        return redirect('auth')


def auth(request):
    auth_form = AuthenticationForm()
    if request.POST:
        auth_form = AuthenticationForm(request, request.POST)
        if auth_form.is_valid():
            user = authenticate(username=auth_form.data['username'],
                                password=auth_form.data['password'])
            login(request, user)
            return redirect('new_post')
    return render(request, 'main/login.html', {'auth_form': auth_form})


def logout(request):
    auth_logout(request)
    return redirect('index')