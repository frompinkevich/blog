# coding: utf-8
from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Posts(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    slug = models.CharField(max_length=1000, verbose_name='Слаг', blank=True, null=True)
    short_body = models.TextField(verbose_name='Краткое содержание')
    body = models.TextField(verbose_name='Содержание')
    author = models.ForeignKey(User, verbose_name='Автор')
    created = models.DateField(verbose_name='Дата добавления')
    image = models.ImageField(verbose_name='Изображение', upload_to='static/post/%Y/%m/')