from django.contrib import admin
from apps.main.models import Posts

# Register your models here.


class PostsAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'created')
    list_filter = ('created',)
    search_fields = ['title', 'author__username']

admin.site.register(Posts, PostsAdmin)