# -*- coding: utf-8 -*-
from django import forms

from apps.main.models import Posts


class NewPostForm(forms.ModelForm):
    class Meta:
        model = Posts
        fields = ['title', 'body', 'image']